#
# Centos 7 with activiti Dockerfile
#
# Pull base image.
FROM bvuser/centos7
MAINTAINER Pincemail Sebastien "pincemail.sebastien@gmail.com"

RUN yum install -y unzip

EXPOSE 8080

ENV TOMCAT_VERSION 8.0.14
ENV ACTIVITI_VERSION 5.16.4
ENV POSTGRES_CONNECTOR_JAVA_VERSION 9.3-1103 
RUN mkdir -p /appli/activiti

RUN wget http://archive.apache.org/dist/tomcat/tomcat-8/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz -O /tmp/catalina.tar.gz
RUN wget https://github.com/Activiti/Activiti/releases/download/activiti-${ACTIVITI_VERSION}/activiti-${ACTIVITI_VERSION}.zip -O /tmp/activiti.zip
# Unpack
RUN tar xzf /tmp/catalina.tar.gz -C /appli
RUN mv /appli/apache-tomcat-${TOMCAT_VERSION} /appli/tomcat

RUN rm /tmp/catalina.tar.gz

RUN unzip /tmp/activiti.zip -d /appli/activiti

# Remove unneeded apps
RUN rm -rf /appli/tomcat/webapps/examples
RUN rm -rf /appli/tomcat/webapps/docs
RUN rm -rf /appli/tomcat/webapps/ROOT
RUN  rm -rf /appli/tomcat/webapps/host-manager
RUN  rm -rf /appli/tomcat/webapps/manager

# To install jar files first we need to deploy war files manually
RUN unzip /appli/activiti/activiti-${ACTIVITI_VERSION}/wars/activiti-explorer.war -d /appli/tomcat/webapps/activiti-explorer
RUN unzip /appli/activiti/activiti-${ACTIVITI_VERSION}/wars/activiti-rest.war -d /appli/tomcat/webapps/activiti-rest

RUN rm -f /appli/tomcat/webapps/activiti-explorer/WEB-INF/classes/engine.properties
RUN rm -f /appli/tomcat/webapps/activiti-rest/WEB-INF/classes/engine.properties

COPY assets/config/tomcat/engine-exp.properties /appli/tomcat/webapps/activiti-explorer/WEB-INF/classes/engine.properties
COPY assets/config/tomcat/engine-res.properties /appli/tomcat/webapps/activiti-rest/WEB-INF/classes/engine.properties

# Add postgresql connector to application
RUN wget https://jdbc.postgresql.org/download/postgresql-${POSTGRES_CONNECTOR_JAVA_VERSION}.jdbc3.jar -O /tmp/postgresql-${POSTGRES_CONNECTOR_JAVA_VERSION}.jdbc3.jar
COPY postgresql-${POSTGRES_CONNECTOR_JAVA_VERSION}.jdbc3.jar /appli/tomcat/webapps/activiti-rest/WEB-INF/lib/postgresql-${POSTGRES_CONNECTOR_JAVA_VERSION}.jdbc3.jar
COPY  postgresql-${POSTGRES_CONNECTOR_JAVA_VERSION}.jdbc3.jar /appli/tomcat/webapps/activiti-explorer/WEB-INF/lib/postgresql-${POSTGRES_CONNECTOR_JAVA_VERSION}.jdbc3.jar

# Add roles
ADD assets /assets
RUN chmod +x /assets/init
COPY assets/config/tomcat/tomcat-users.xml /appli/tomcat/conf/tomcat-users.xml 

VOLUME ["/appli"]

CMD ["/assets/init"]

